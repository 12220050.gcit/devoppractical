const path = require('path') // path module provides a way of working directories and file paths

// LOG IN PAGE
exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../','views', 'login.html'))
}

// SIGN UP FORM
exports.getSignupForm = (req, res) => {
    res.sednFile(path.join(__dirname, '../', 'views', 'signup.html'))
}

// HOME PAGE
exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'view', 'dashboard.html'))
}
// sendFile() to serve the static files